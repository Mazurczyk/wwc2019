package sda.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpClient {

    public String makeRequest(String requestUrl) {
        URL url;
        HttpURLConnection httpURLConnection = null;
        try {
            url = new URL(requestUrl);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.addRequestProperty("User-Agent", "Mozilla/5.0");
            httpURLConnection.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return readResponse(httpURLConnection);
    }

    private String readResponse(HttpURLConnection httpURLConnection) {
        BufferedReader in;
        String inputLine;
        StringBuffer content = null;
        try {
            in = new BufferedReader(new InputStreamReader((InputStream) httpURLConnection.getContent()));
            content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content.toString();
    }
}
