package sda.model;

import com.google.gson.annotations.SerializedName;

public class Team {
    private Integer id;
    private String country;
    @SerializedName("alternate_name")
    private String alternateName;
    @SerializedName("fifa_code")
    private String fifaCode;
    @SerializedName("group_id")
    private Integer groupID;
    @SerializedName("group_letter")
    private String groupLetter;
    private Integer wins;
    private Integer draws;
    private Integer losses;
    @SerializedName("games_played")
    private Integer gamesPlayed;
    private Integer points;
    @SerializedName("goals_for")
    private Integer goalsFor;
    @SerializedName("goals_against")
    private Integer goalsAgainst;
    @SerializedName("goal_differential")
    private Integer goalDifferential;

    public Integer getId() {
        return id;
    }

    public String getCountry() {
        return country;
    }

    public String getAlternateName() {
        return alternateName;
    }

    public String getFifaCode() {
        return fifaCode;
    }

    public Integer getGroupID() {
        return groupID;
    }

    public String getGroupLetter() {
        return groupLetter;
    }

    public Integer getWins() {
        return wins;
    }

    public Integer getDraws() {
        return draws;
    }

    public Integer getLosses() {
        return losses;
    }

    public Integer getGamesPlayed() {
        return gamesPlayed;
    }

    public Integer getPoints() {
        return points;
    }

    public Integer getGoalsFor() {
        return goalsFor;
    }

    public Integer getGoalsAgainst() {
        return goalsAgainst;
    }

    public Integer getGoalDifferential() {
        return goalDifferential;
    }

    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", country='" + country + '\'' +
                ", alternateName='" + alternateName + '\'' +
                ", fifaCode='" + fifaCode + '\'' +
                ", groupID=" + groupID +
                ", groupLetter='" + groupLetter + '\'' +
                ", wins=" + wins +
                ", draws=" + draws +
                ", losses=" + losses +
                ", gamesPlayed=" + gamesPlayed +
                ", points=" + points +
                ", goalsFor=" + goalsFor +
                ", goalsAgainst=" + goalsAgainst +
                ", goalsDifferential=" + goalDifferential +
                '}';
    }
}
