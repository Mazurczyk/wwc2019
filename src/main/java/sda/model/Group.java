package sda.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Group {
    private Integer id;
    private  String letter;
    @SerializedName("ordered_teams")
    private List<Team> orderedTeams;

    public Integer getId() {
        return id;
    }

    public String getLetter() {
        return letter;
    }

    public List<Team> getOrderedTeams() {
        return orderedTeams;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", letter='" + letter + '\'' +
                ", orderedTeams=" + orderedTeams +
                '}';
    }
}
