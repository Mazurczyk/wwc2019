package sda.model;

public class MatchTeam {
    private String country;
    private String code;
    private Integer goals;
    private Integer penalties;

    public String getCountry() {
        return country;
    }

    public String getCode() {
        return code;
    }

    public Integer getGoals() {
        return goals;
    }

    public Integer getPenalties() {
        return penalties;
    }

    @Override
    public String toString() {
        return "MatchTeam{" +
                "country='" + country + '\'' +
                ", code='" + code + '\'' +
                ", goals=" + goals +
                ", penalties=" + penalties +
                '}';
    }
}
