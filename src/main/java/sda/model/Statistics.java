package sda.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Statistics {
    private String country;
    @SerializedName("attempts_on_goal")
    private Integer attemptsOnGoal;
    @SerializedName("on_target")
    private Integer onTarget;
    @SerializedName("off_target")
    private Integer offTarget;
    private Integer blocked;
    private Integer corners;
    private Integer offsides;
    @SerializedName("ball_possession")
    private Integer ballPossession;
    @SerializedName("pass_accuracy")
    private Integer passAccuracy;
    @SerializedName("num_passes")
    private Integer numPasses;
    @SerializedName("passes_completed")
    private Integer passesCompleted;
    @SerializedName("distance_covered")
    private Integer distanceCovered;
    private Integer tackles;
    private Integer clearances;
    @SerializedName("yellow_cards")
    private int yellowCards;
    @SerializedName("red_cards")
    private int redCards;
    @SerializedName("fouls_committed")
    private Integer foulsCommitted;
    private String tactics;
    @SerializedName("starting_eleven")
    private List<Footballer> startingEleven;
    private List<Footballer> substitutes;

    public String getCountry() {
        return country;
    }

    public Integer getAttemptsOnGoal() {
        return attemptsOnGoal;
    }

    public Integer getOnTarget() {
        return onTarget;
    }

    public Integer getOffTarget() {
        return offTarget;
    }

    public Integer getBlocked() {
        return blocked;
    }

    public Integer getCorners() {
        return corners;
    }

    public Integer getOffsides() {
        return offsides;
    }

    public Integer getBallPossession() {
        return ballPossession;
    }

    public Integer getPassAccuracy() {
        return passAccuracy;
    }

    public Integer getNumPasses() {
        return numPasses;
    }

    public Integer getPassesCompleted() {
        return passesCompleted;
    }

    public Integer getDistanceCovered() {
        return distanceCovered;
    }

    public Integer getTackles() {
        return tackles;
    }

    public Integer getClearances() {
        return clearances;
    }

    public Integer getYellowCards() {
        return yellowCards;
    }

    public Integer getRedCards() {
        return redCards;
    }

    public Integer getFoulsCommitted() {
        return foulsCommitted;
    }

    public String getTactics() {
        return tactics;
    }

    public List<Footballer> getStartingEleven() {
        return startingEleven;
    }

    public List<Footballer> getSubstitutes() {
        return substitutes;
    }

    @Override
    public String toString() {
        return "Statistics{" +
                "country='" + country + '\'' +
                ", attemptsOnGoal=" + attemptsOnGoal +
                ", onTarget=" + onTarget +
                ", offTarget=" + offTarget +
                ", blocked=" + blocked +
                ", corners=" + corners +
                ", offsides=" + offsides +
                ", ballPossession=" + ballPossession +
                ", passAccuracy=" + passAccuracy +
                ", numPasses=" + numPasses +
                ", passesCompleted=" + passesCompleted +
                ", distanceCovered=" + distanceCovered +
                ", tackles=" + tackles +
                ", clearances=" + clearances +
                ", yellowCards=" + yellowCards +
                ", redCards=" + redCards +
                ", foulsCommitted=" + foulsCommitted +
                ", tactics='" + tactics + '\'' +
                ", startingEleven=" + startingEleven +
                ", substitutes=" + substitutes +
                '}';
    }
}
