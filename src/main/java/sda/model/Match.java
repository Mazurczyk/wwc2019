package sda.model;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;
import java.util.List;

public class Match {
    private String venue;
    private String location;
    private String status;
    private String time;
    @SerializedName("fifa_id")
    private String fifaID;
    private Weather weather;
    private String attendance;
    private String[] officials;
    @SerializedName("stage_name")
    private String stageName;
    @SerializedName("home_team_country")
    private String homeTeamCountry;
    @SerializedName("away_team_country")
    private String awayTeamCountry;
    @SerializedName("datetime")
    private String dateTime;
    private String winner;
    @SerializedName("winner_code")
    private String winnerCode;
    @SerializedName("home_team")
    private MatchTeam homeTeam;
    @SerializedName("away_team")
    private MatchTeam awayTeam;
    @SerializedName("home_team_events")
    private List<Events> homeTeamEvents;
    @SerializedName("away_team_events")
    private List<Events> awayTeamEvents;
    @SerializedName("home_team_statistics")
    private Statistics homeTeamStatistics;
    @SerializedName("away_team_statistics")
    private Statistics awayTeamStatistics;
    @SerializedName("last_event_update_at")
    private String lastEventUpdateAt;
    @SerializedName("last_score_update_at")
    private String lastScoreUpdateAt;

    public String getVenue() {
        return venue;
    }

    public String getLocation() {
        return location;
    }

    public String getStatus() {
        return status;
    }

    public String getTime() {
        return time;
    }

    public String getFifaID() {
        return fifaID;
    }

    public Weather getWeather() {
        return weather;
    }

    public String getAttendance() {
        return attendance;
    }

    public String[] getOfficials() {
        return officials;
    }

    public String getStageName() {
        return stageName;
    }

    public String getHomeTeamCountry() {
        return homeTeamCountry;
    }

    public String getAwayTeamCountry() {
        return awayTeamCountry;
    }

    public String getDateTime() {
        return dateTime;
    }

    public String getWinner() {
        return winner;
    }

    public String getWinnerCode() {
        return winnerCode;
    }

    public MatchTeam getHomeTeam() {
        return homeTeam;
    }

    public MatchTeam getAwayTeam() {
        return awayTeam;
    }

    public List<Events> getHomeTeamEvents() {
        return homeTeamEvents;
    }

    public List<Events> getAwayTeamEvents() {
        return awayTeamEvents;
    }

    public Statistics getHomeTeamStatistics() {
        return homeTeamStatistics;
    }

    public Statistics getAwayTeamStatistics() {
        return awayTeamStatistics;
    }

    public String getLastEventUpdateAt() {
        return lastEventUpdateAt;
    }

    public String getLastScoreUpdateAt() {
        return lastScoreUpdateAt;
    }

    @Override
    public String toString() {
        return "Match{" +
                "venue='" + venue + '\'' +
                ", location='" + location + '\'' +
                ", status='" + status + '\'' +
                ", time='" + time + '\'' +
                ", fifaID='" + fifaID + '\'' +
                ", weather=" + weather +
                ", attendance='" + attendance + '\'' +
                ", officials=" + Arrays.toString(officials) +
                ", stageName='" + stageName + '\'' +
                ", homeTeamCountry='" + homeTeamCountry + '\'' +
                ", awayTeamCountry='" + awayTeamCountry + '\'' +
                ", dateTime='" + dateTime + '\'' +
                ", winner='" + winner + '\'' +
                ", winnerCode='" + winnerCode + '\'' +
                ", homeTeam=" + homeTeam +
                ", awayTeam=" + awayTeam +
                ", homeTeamEvents=" + homeTeamEvents +
                ", awayTeamEvents=" + awayTeamEvents +
                ", homeTeamStatistics=" + homeTeamStatistics +
                ", awayTeamStatistics=" + awayTeamStatistics +
                ", lastEventUpdateAt='" + lastEventUpdateAt + '\'' +
                ", lastScoreUpdateAt='" + lastScoreUpdateAt + '\'' +
                '}';
    }
}
