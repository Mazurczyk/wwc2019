package sda.model;

import com.google.gson.annotations.SerializedName;

public class Events {
    private Integer id;
    @SerializedName("type_of_event")
    private String typeOfEvent;
    private String player;
    private String time;

    public Integer getId() {
        return id;
    }

    public String getTypeOfEvent() {
        return typeOfEvent;
    }

    public String getPlayer() {
        return player;
    }

    public String getTime() {
        return time;
    }

    @Override
    public String toString() {
        return "Events{" +
                "id=" + id +
                ", typeOfEvent='" + typeOfEvent + '\'' +
                ", player='" + player + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
