package sda.model;

import com.google.gson.annotations.SerializedName;

public class Weather {
    private String humidity;
    @SerializedName("temp_celsius")
    private String tempCelsius;
    @SerializedName("temp_farenheit")
    private String tempFarenheit;
    @SerializedName("wind_speed")
    private String windSpeed;
    private String description;

    public String getHumidity() {
        return humidity;
    }

    public String getTempCelsius() {
        return tempCelsius;
    }

    public String getTempFarenheit() {
        return tempFarenheit;
    }

    public String getWindSpeed() {
        return windSpeed;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "humidity='" + humidity + '\'' +
                ", tempCelsius='" + tempCelsius + '\'' +
                ", tempFarenheit='" + tempFarenheit + '\'' +
                ", windSpeed='" + windSpeed + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
