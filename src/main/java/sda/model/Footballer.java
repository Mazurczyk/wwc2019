package sda.model;

import com.google.gson.annotations.SerializedName;

public class Footballer {
    private String name;
    private Boolean captain;
    @SerializedName("shirt_number")
    private Integer shirtNumber;
    private String position;

    public String getName() {
        return name;
    }

    public Boolean getCaptain() {
        return captain;
    }

    public Integer getShirtNumber() {
        return shirtNumber;
    }

    public String getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return "StartingEleven{" +
                "name='" + name + '\'' +
                ", captain=" + captain +
                ", shirtNumber=" + shirtNumber +
                ", position='" + position + '\'' +
                '}';
    }
}
