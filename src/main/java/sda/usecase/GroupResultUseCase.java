package sda.usecase;

import sda.model.Group;

import java.util.List;

//1. Group results:
//https://worldcup.sfg.io/teams/group_results
//- Znajdź wszystkie zespoły, które nie zremisowały ani jednego meczu
//- Znajdź wszystkie zespoły, które wygrały co najmniej 3 mecze
//- Znajdź wszystkie zespoły z conajmniej 2 przegranymi na koncie
//- Znajdź wszystkie zespoły z większą liczbą straconych bramek niż strzelonych
//Collap

public class GroupResultUseCase {
    public static void findTeamsWithoutDraws(List<Group> groups) {
        groups.stream()
                .flatMap(group -> group.getOrderedTeams().stream())
                .filter(team -> team.getDraws() == 0)
                .forEach(System.out::println);
    }

    public static void findTeamsWithAtLeast3Wins(List<Group> groups) {
        groups.stream()
                .flatMap(group -> group.getOrderedTeams().stream())
                .filter(team -> team.getWins() >= 3)
                .forEach(System.out::println);
    }

    public static void findTeamsWithAtLeast2Losses(List<Group> groups) {
        groups.stream()
                .flatMap(group -> group.getOrderedTeams().stream())
                .filter(team -> team.getLosses() >= 2)
                .forEach(System.out::println);
    }

    public static void findTeamsWithNegativeGoalDifferential(List<Group> groups) {
        groups.stream()
                .flatMap(group -> group.getOrderedTeams().stream())
                .filter(team -> team.getGoalDifferential() < 0)
                .forEach(System.out::println);
    }
}
