package sda.usecase;

import sda.model.Events;
import sda.model.Footballer;
import sda.model.Match;
import sda.model.Statistics;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

//2. Mecze
//https://worldcup.sfg.io/matches
//- Znajdź wszystkie zespoły, których piłkarze otrzymali czerwoną kartkę
//- Znajdź wszystkie zespołu których zawodnicy podczas meczu nie otrzymali ani jednej żółtej i czerwonej kartki
//- Znajdź zespoły które grały w ustawieniu 4-3-3
//- Znajdź zespoły z posiadaniem piłki na poziomie ponad 65%
//- Zwróć wszystkie piłkarki które rozpoczynały mecz w podstawowej jedenastce
//- Zwróć wszystkie piłkarki które weszły na boisku w trakcie meczu
//- xxZwróć zwycięzców każdego z meczów
//- Zwróć wszystkie zespoły które podczas meczu dokonały ponad 30 strzałów na bramkę

public class MatchesUseCase {

    public static void findTeamsWithRedCard(List<Match> matches) {
        matches.stream()
                .flatMap(match -> Arrays.asList(match.getAwayTeamStatistics(),
                        match.getHomeTeamStatistics()).stream())
                .filter(statistics -> statistics.getRedCards() != 0)
                .map(Statistics::getCountry)
                .distinct()
                .forEach(System.out::println);
    }

    public static void findTeamsWithoutAnyCard(List<Match> matches) {
        matches.stream()
                .flatMap(match -> Arrays.asList(match.getHomeTeamStatistics(),
                        match.getAwayTeamStatistics()).stream())
                .filter(statistics -> statistics.getRedCards() == 0 &&
                        statistics.getYellowCards() == 0)
                .map(Statistics::getCountry)
                .distinct()
                .forEach(System.out::println);
    }

    public static void findTeamsWithTactics433(List<Match> matches) {
        matches.stream()
                .flatMap(match -> Arrays.asList(match.getHomeTeamStatistics(),
                        match.getAwayTeamStatistics()).stream())
                .filter(statistics -> statistics.getTactics().equals("4-3-3"))
                .map(Statistics::getCountry)
                .distinct()
                .forEach(System.out::println);
    }

    public static void findTeamsWithBallPossessionMoreThan65(List<Match> matches) {
        matches.stream()
                .flatMap(match -> Arrays.asList(match.getHomeTeamStatistics(),
                        match.getAwayTeamStatistics()).stream())
                .filter(statistics -> statistics.getBallPossession() > 65)
                .map(Statistics::getCountry)
                .distinct()
                .forEach(System.out::println);
    }

    public static void findFootballersFromStartingEleven(List<Match> matches) {
        matches.stream()
                .flatMap(match -> Stream.concat(match.getHomeTeamStatistics().getStartingEleven().stream(),
                        match.getAwayTeamStatistics().getStartingEleven().stream()))
                .map(Footballer::getName)
                .distinct()
                .forEach(System.out::println);
    }

    public static void findCaptainsOfTeams(List<Match> matches) {
        matches.stream()
                .flatMap(match -> Stream.concat(match.getHomeTeamStatistics().getStartingEleven().stream(),
                        match.getAwayTeamStatistics().getStartingEleven().stream()))
                .filter(footballer -> footballer.getCaptain())
                .map(Footballer::getName)
                .distinct()
                .forEach(System.out::println);

    }

    public static void findFootballersWhichWasSubstitutedOn(List<Match> matches) {
        matches.stream()
                .flatMap(match -> Stream.concat(match.getHomeTeamEvents().stream(),
                        match.getAwayTeamEvents().stream()))
                .filter(events -> events.getTypeOfEvent().equals("substitution-in"))
                .map(Events::getPlayer)
                .distinct()
                .forEach(System.out::println);
    }

    public static void findWinnersOfEachMatch(List<Match> matches) {
        matches.stream()
                .map(Match::getWinner)
                .filter(winner -> !winner.equals("Draw"))
                .forEach(System.out::println);
    }

    public static void findTeamsWhichStrikeOnAndOffTargetMoreThan30Times(List<Match> matches) {
        matches.stream()
                .flatMap(match -> Stream.of(match.getHomeTeamStatistics(),
                        match.getAwayTeamStatistics()))
                .filter(statistics -> statistics.getOnTarget() + statistics.getOffTarget() > 30)
                .map(Statistics::getCountry)
                .distinct()
                .forEach(System.out::println);
    }
}
