package sda.command;

import java.util.List;

public interface Command<T> {
    List<T> fetch();
}
