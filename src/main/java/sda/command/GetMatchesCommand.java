package sda.command;

import sda.model.Match;
import sda.network.HttpClient;

public class GetMatchesCommand extends BaseCommand<Match> implements Command<Match> {

    public GetMatchesCommand(HttpClient httpClient) {
        super(httpClient);
    }

    @Override
    String getURL() {
        return "https://worldcup.sfg.io/matches";
    }

    @Override
    Class<Match[]> getClassToDeserialise() {
        return Match[].class;
    }

}
