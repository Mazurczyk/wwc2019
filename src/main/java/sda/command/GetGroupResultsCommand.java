package sda.command;

import sda.model.Group;
import sda.network.HttpClient;

public class GetGroupResultsCommand extends BaseCommand<Group> implements Command<Group> {

    public GetGroupResultsCommand(HttpClient httpClient) {
        super(httpClient);
    }

    @Override
    String getURL() {
        return "https://worldcup.sfg.io/teams/group_results";
    }

    @Override
    Class<Group[]> getClassToDeserialise() {
        return Group[].class;
    }
}
