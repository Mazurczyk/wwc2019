package sda.command;

import com.google.gson.Gson;
import sda.network.HttpClient;

import java.util.Arrays;
import java.util.List;

public abstract class BaseCommand<T> implements Command<T> {

    protected HttpClient httpClient;

    public BaseCommand(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    abstract String getURL();

    abstract Class<T[]> getClassToDeserialise();

    public List<T> fetch() {
        Gson gson = new Gson();
        String response = httpClient.makeRequest(getURL());
        T[] fromJson = gson.fromJson(response, getClassToDeserialise());
        return Arrays.asList(fromJson);
    }
}
