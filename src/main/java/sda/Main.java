package sda;

import sda.command.Command;
import sda.command.GetMatchesCommand;
import sda.model.Match;
import sda.network.HttpClient;
import sda.usecase.MatchesUseCase;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        HttpClient httpClient = new HttpClient();
//        Command<Group> getGroupResultsCommand = new GetGroupResultsCommand(httpClient);
//        List<Group> groupsResults = getGroupResultsCommand.fetch();
//        System.out.println(groupsResults);
//        GroupResultUseCase.findTeamsWithoutDraws(groupsResults);
//        GroupResultUseCase.findTeamsWithAtLeast3Wins(groupsResults);
//        GroupResultUseCase.findTeamsWithAtLeast2Losses(groupsResults);
//        GroupResultUseCase.findTeamsWithNegativeGoalDifferential(groupsResults);
        Command<Match> getMatchesCommand = new GetMatchesCommand(httpClient);
        List<Match> matches = getMatchesCommand.fetch();
//        MatchesUseCase.findTeamsWithRedCard(matches);
//        MatchesUseCase.findTeamsWithoutAnyCard(matches);
//        MatchesUseCase.findTeamsWithTactics433(matches);
//        MatchesUseCase.findTeamsWithBallPossessionMoreThan65(matches);
//        MatchesUseCase.findFootballersFromStartingEleven(matches);
//        MatchesUseCase.findCaptainsOfTeams(matches);
//        MatchesUseCase.findFootballersWhichWasSubstitutedOn(matches);
//        MatchesUseCase.findWinnersOfEachMatch(matches);
        MatchesUseCase.findTeamsWhichStrikeOnAndOffTargetMoreThan30Times(matches);

    }
}
